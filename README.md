Create and delete project mailing lists.


    $ ./mkml --help
    usage: mkml [-h] [--config CONFIG] [--dry-run] [--owner OWNER] name

    Creates a project mailing list.

    positional arguments:
      name             The name of the new mailing list

    optional arguments:
      -h, --help       show this help message and exit
      --config CONFIG  Alternative config file, default: config.json
      --dry-run        Only check for validness of the request and exit.
      --owner OWNER    The email address of the owner of the mailinglist


    $ ./rmml --help
    usage: rmml [-h] [--dry-run] name

    Deletes a mailing list created with ezmlm-make, including the corresponding
    .qmail files

    positional arguments:
      name        The name of the new mailing list as used before the @ sign. This
                  will determine the .qmail file used to find and delete the list
                  directory.

    optional arguments:
      -h, --help  show this help message and exit
      --dry-run   Only show what would be deleted.
